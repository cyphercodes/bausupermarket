<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "product".
 *
 * @property integer $id
 * @property string $title
 * @property double $price
 * @property integer $expiry_date
 * @property double $tax
 * @property double $profit_margin
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Basketitem[] $basketitems
 */
class Product extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'product';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['title', 'price', 'profit_margin'], 'required'],
            [['title'], 'string', 'max' => 255],
            [['price', 'tax', 'profit_margin'], 'number'],
            [['created_at', 'updated_at'], 'integer'],
            ['expiry_date', 'date', 'timestampAttribute' => 'expiry_date'],
            [['expiry_date'], 'default', 'value' => null],
        ];
    }

    public function behaviors() {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'price' => 'Price',
            'expiry_date' => 'Expiry Date',
            'tax' => 'Tax',
            'profit_margin' => 'Profiit Margin',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'customerprice' => 'Price',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBasketitems() {
        return $this->hasMany(Basketitem::className(), ['product_id' => 'id']);
    }

    public function getCustomerprice() {
        // price: 5000 - profit margin: 30
         $multiplier = ($this->profit_margin + 100) / 100;
        return $this->price * $multiplier;
    }

    public function setCustomerprice($price) {
        $this->customerprice = $price;
    }

    public function getTaxamount() {
        if (isset($this->tax) && !empty($this->tax)) {
            // price: 5000 - tax: 20
            $multiplier = $this->tax / 100;
            return $this->price * $multiplier;
        }
        return 0;
    }

}
