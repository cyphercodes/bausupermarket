<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 */
class SignupForm extends Model {

    public $email;
    public $password;
    public $firstname;
    public $lastname;

    /**
     * @return array the validation rules.
     */
    public function rules() {
        return [
            // username and password are both required
            [['email', 'password', 'firstname', 'lastname'], 'required'],
        ];
    }

    public function attributeLabels() {
        return [
            'username' => 'Email',
            'password'=>'Password',
            'firstname'=>'First Name',
            'lastname'=>'Last Name'
        ];
    }

    /**
     * Logs in a user using the provided username and password.
     * @return boolean whether the user is logged in successfully
     */
    public function signup() {
        if ($this->validate()) {
            $user = new User();
            $user->attributes = $this->attributes;
            $user->setPassword($this->password);
            $user->generateAuthKey();
            $user->type = User::TYPE_USER;
            if($user->save()){
                Yii::$app->user->login($user, 3600 * 24 * 30);
                return true;
            }
        }
        return false;
    }

}
