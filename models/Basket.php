<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "basket".
 *
 * @property integer $id
 * @property double $totalcost
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $user_id
 *
 * @property User $user
 * @property Basketitem[] $basketitems
 */
class Basket extends \yii\db\ActiveRecord {

    const STATUS_PENDING = 0;
    const STATUS_COMPLETE = 10;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'basket';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['user_id'], 'required'],
            [['totalcost'], 'number'],
            [['status', 'created_at', 'updated_at', 'user_id'], 'integer'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            ['status', 'default', 'value' => self::STATUS_PENDING],
            ['status', 'in', 'range' => [self::STATUS_PENDING, self::STATUS_COMPLETE]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'totalcost' => 'Totalcost',
            'status' => 'Status',
            'created_at' => 'Create At',
            'updated_at' => 'Updated At',
            'user_id' => 'User ID',
        ];
    }

    public function behaviors() {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBasketitems() {
        return $this->hasMany(Basketitem::className(), ['basket_id' => 'id']);
    }

    public function totalPrice() {
        $total = 0;
        foreach ($this->basketitems as $item) {
            $total+= $item->price;
        }
        return $total;
    }

    public function getTax() {
        $tax = 0;
        foreach ($this->basketitems as $item) {
            $tax += $item->tax;
        }
        return $tax;
    }

    public function getProfit() {
        $profit = 0;
        foreach ($this->basketitems as $item) {
            $profit += $item->profit;
        }
        return $profit;
    }

    public function getItemcount() {
        $itemCount = 0;
        foreach ($this->basketitems as $item) {
            $itemCount += $item->quantity;
        }
  
        return $itemCount;
    }

}
