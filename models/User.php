<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $auth_key
 * @property string $password_hash
 * @property string $email
 * @property integer $type
 * @property string $firstname
 * @property string $lastname
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Basket[] $baskets
 */
class User extends ActiveRecord implements IdentityInterface {

    const TYPE_ADMIN = 0;
    const TYPE_DATAENTRY = 10;
    const TYPE_USER = 20;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['auth_key', 'password_hash', 'email'], 'required'],
            [['type', 'created_at', 'updated_at'], 'integer'],
            [['auth_key'], 'string', 'max' => 32],
            [['password_hash', 'firstname', 'lastname', 'email'], 'string', 'max' => 255],
            [['email'], 'unique'],
            ['type', 'default', 'value' => self::TYPE_USER],
            ['type', 'in', 'range' => [self::TYPE_ADMIN, self::TYPE_DATAENTRY, self::TYPE_USER]],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'ID',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'email' => 'Email',
            'type' => 'Type',
            'firstname' => 'Firstname',
            'lastname' => 'Lastname',
            'fullname' => 'Name',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function getBaskets() {
        return $this->hasMany(Basket::className(), ['user_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id) {
        return static::findOne(['id' => $id]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null) {
        return static::findOne(['auth_key' => $token]);
    }

    public static function findByEmail($email) {
        return static::findOne(['email' => $email]);
    }

    /**
     * @inheritdoc
     */
    public function getId() {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey() {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey) {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password) {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password) {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey() {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    public function getFullname() {
        return $this->fullName();
    }

    public function setFullName($value) {
        $this->fullname = $value;
    }

    public function beforeValidate() {
        if ($this->isNewRecord) {
            $this->firstname = ucwords($this->firstname);
            $this->lastname = ucwords($this->lastname);
        }
        return parent::beforeValidate();
    }

    public function hasActiveBasket() {
        if ($basket = Basket::find()->where('user_id = :user_id AND status = :status', [':user_id' => $this->id, ':status' => Basket::STATUS_PENDING])->one()) {
            return $basket;
        }
        return false;
    }

    public function isAdmin() {
        return $this->type == self::TYPE_ADMIN;
    }

}
