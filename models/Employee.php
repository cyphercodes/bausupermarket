<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "employee".
 *
 * @property integer $id
 * @property string $firstname
 * @property string $lastname
 * @property string $gender
 * @property double $salary
 * @property integer $created_at
 * @property integer $updated_at
 */
class Employee extends ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'employee';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['firstname'], 'required'],
            [['created_at', 'updated_at'], 'integer'],
            [['salary'], 'number'],
            [['gender'], 'string', 'max' => 55],
            [['firstname', 'lastname'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'firstname' => 'First Name',
            'lastname' => 'Last Name',
            'gender' => 'Gender',
            'salary' => 'Salary',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function beforeValidate() {
        if ($this->isNewRecord) {
            $this->firstname = ucwords($this->firstname);
            $this->lastname = ucwords($this->lastname);
        }
        return parent::beforeValidate();
        // $model->save();
    }

}
