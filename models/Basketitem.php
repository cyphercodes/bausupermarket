<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use app\models\User;

/**
 * This is the model class for table "basketitem".
 *
 * @property integer $id
 * @property integer $quantity
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $product_id
 * @property integer $basket_id
 *
 * @property Basket $basket
 * @property Product $product
 */
class Basketitem extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'basketitem';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['quantity', 'product_id', 'basket_id'], 'required'],
            [['quantity', 'created_at', 'updated_at', 'product_id', 'basket_id'], 'integer'],
            [['quantity'], 'default', 'value' => 1],
            [['basket_id'], 'exist', 'skipOnError' => true, 'targetClass' => Basket::className(), 'targetAttribute' => ['basket_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'quantity' => 'Quantity',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'product_id' => 'Product ID',
            'basket_id' => 'Basket ID',
        ];
    }

    public function behaviors() {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBasket() {
        return $this->hasOne(Basket::className(), ['id' => 'basket_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct() {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    public function beforeValidate() {
        if ($basket = Yii::$app->user->identity->hasActiveBasket()) {
            $this->basket_id = $basket->id;
        } else {
            $basket = new Basket();
            $basket->user_id = Yii::$app->user->identity->id;
            $basket->save();
            $this->basket_id = $basket->getPrimaryKey();
        }
        return parent::beforeValidate();
    }

    public function getPrice() { // Price for the customers
        return $this->product->customerprice * $this->quantity;
    }

    public function getOriginalPrice() { // Price for the supermarket
        return $this->product->price * $this->quantity;
    }

    public function getProfit() { // Profit
        return $this->price - $this->originalprice;
    }

    public function getTax() {
        return $this->product->taxamount * $this->quantity;
    }

}
