BAU Supermarket
============================


REQUIREMENTS
------------

The minimum requirement by this project template that your Web server supports PHP 5.4.0.

CONFIGURATION
-------------

### Database

Configure database in `config/db.php`, for example:

```php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=bulkdomainchecker',
    'username' => 'root',
    'password' => '1234',
    'charset' => 'utf8',
];
```

Apply database migrations

### Configure Environment

Add a virtual host to apache:

```
<VirtualHost *:80>
    ServerName bausupermarket.local
    # use correct directory
    DocumentRoot "c:/wamp/www/supermarket/bausupermarket/web"

    # use correct directory
    <Directory "c:/wamp/www/supermarket/bausupermarket/web">
        # use mod_rewrite for pretty URL support
		RewriteEngine on
		# If a directory or a file exists, use the request directly
		RewriteCond %{REQUEST_FILENAME} !-f
		RewriteCond %{REQUEST_FILENAME} !-d
		# Otherwise forward the request to index.php
		RewriteRule . index.php
	</Directory>
</VirtualHost>
```


Add this line to the hosts file
On Windows: C:\Windows\System32\drivers\etc\hosts
```
127.0.0.1	bausupermarket.local
```

### Users

Admin:
admin@bausupermarket.local
password123


Data Entry Operator:
dataentry@bausupermarket.local
password123


Customer
customer@bausupermarket.local
password123




Created By Rawad Salhab