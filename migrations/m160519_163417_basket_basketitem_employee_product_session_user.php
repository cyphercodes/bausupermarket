<?php

use yii\db\Schema;
use yii\db\Migration;

class m160519_163417_basket_basketitem_employee_product_session_user extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
             $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%basket}}', [
            'id'=> Schema::TYPE_PK.'',
            'totalcost'=> Schema::TYPE_FLOAT.' NOT NULL',
            'status'=> Schema::TYPE_SMALLINT.'(6) NOT NULL',
            'created_at'=> Schema::TYPE_INTEGER.'(11) NOT NULL',
            'updated_at'=> Schema::TYPE_INTEGER.'(11) NOT NULL',
            'user_id'=> Schema::TYPE_INTEGER.'(11) NOT NULL',
            ], $tableOptions);

        $this->createIndex('id_UNIQUE', '{{%basket}}', 'id', 1);
        $this->createIndex('fk_basket_user1_idx', '{{%basket}}', 'user_id', 0);
        $this->createTable('{{%basketitem}}', [
            'id'=> Schema::TYPE_PK.'',
            'quantity'=> Schema::TYPE_INTEGER.'(11) NOT NULL',
            'created_at'=> Schema::TYPE_INTEGER.'(11) NOT NULL',
            'updated_at'=> Schema::TYPE_INTEGER.'(11) NOT NULL',
            'product_id'=> Schema::TYPE_INTEGER.'(11) NOT NULL',
            'basket_id'=> Schema::TYPE_INTEGER.'(11) NOT NULL',
            ], $tableOptions);

        $this->createIndex('id_UNIQUE', '{{%basketitem}}', 'id', 1);
        $this->createIndex('fk_basketitem_product_idx', '{{%basketitem}}', 'product_id', 0);
        $this->createIndex('fk_basketitem_basket1_idx', '{{%basketitem}}', 'basket_id', 0);
        $this->createTable('{{%employee}}', [
            'id'=> Schema::TYPE_PK.'',
            'firstname'=> Schema::TYPE_STRING.'(255) NOT NULL',
            'lastname'=> Schema::TYPE_STRING.'(255)',
            'gender'=> Schema::TYPE_STRING.'(55)',
            'salary'=> Schema::TYPE_FLOAT.'',
            'created_at'=> Schema::TYPE_INTEGER.'(11) NOT NULL',
            'updated_at'=> Schema::TYPE_INTEGER.'(11) NOT NULL',
            ], $tableOptions);

        $this->createIndex('id_UNIQUE', '{{%employee}}', 'id', 1);
        $this->createTable('{{%product}}', [
            'id'=> Schema::TYPE_PK.'',
            'title'=> Schema::TYPE_STRING.'(255) NOT NULL',
            'price'=> Schema::TYPE_FLOAT.' NOT NULL',
            'expiry_date'=> Schema::TYPE_INTEGER.'(11) NOT NULL',
            'tax'=> Schema::TYPE_FLOAT.'',
            'profit_margin'=> Schema::TYPE_FLOAT.' NOT NULL',
            'created_at'=> Schema::TYPE_INTEGER.'(11) NOT NULL',
            'updated_at'=> Schema::TYPE_INTEGER.'(11) NOT NULL',
            ], $tableOptions);

        $this->createIndex('id_UNIQUE', '{{%product}}', 'id', 1);
        $this->createTable('{{%session}}', [
            'id'=> Schema::TYPE_CHAR.'(40) NOT NULL',
            'expire'=> Schema::TYPE_INTEGER.'(11)',
            'data'=> Schema::TYPE_BINARY.'',
            ], $tableOptions);

        $this->createTable('{{%user}}', [
            'id'=> Schema::TYPE_PK.'',
            'auth_key'=> Schema::TYPE_STRING.'(32) NOT NULL',
            'password_hash'=> Schema::TYPE_STRING.'(255) NOT NULL',
            'email'=> Schema::TYPE_STRING.'(255) NOT NULL',
            'type'=> Schema::TYPE_SMALLINT.'(6) NOT NULL',
            'firstname'=> Schema::TYPE_STRING.'(255)',
            'lastname'=> Schema::TYPE_STRING.'(255)',
            'updated_at'=> Schema::TYPE_INTEGER.'(11) NOT NULL',
            'created_at'=> Schema::TYPE_INTEGER.'(11) NOT NULL',
            ], $tableOptions);

        $this->createIndex('id_UNIQUE', '{{%user}}', 'id', 1);
        $this->addForeignKey('fk_basket_user_id', '{{%basket}}', 'user_id', 'user', 'id');
        $this->addForeignKey('fk_basketitem_basket_id', '{{%basketitem}}', 'basket_id', 'basket', 'id');
        $this->addForeignKey('fk_basketitem_product_id', '{{%basketitem}}', 'product_id', 'product', 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_basket_user_id', '{{%basket}}');
        $this->dropForeignKey('fk_basketitem_basket_id', '{{%basketitem}}');
        $this->dropForeignKey('fk_basketitem_product_id', '{{%basketitem}}');
        $this->dropTable('{{%basket}}');
        $this->dropTable('{{%basketitem}}');
        $this->dropTable('{{%employee}}');
        $this->dropTable('{{%product}}');
        $this->dropTable('{{%session}}');
        $this->dropTable('{{%user}}');
    }
}
