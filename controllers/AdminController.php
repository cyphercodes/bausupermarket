<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use app\models\Employee;
use yii\data\ActiveDataProvider;
use app\models\Product;
use app\models\Basketitem;
use app\models\Basket;

class AdminController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex() {
        $dataProvider = new ActiveDataProvider([
            'query' => Employee::findBySql('SELECT * FROM employee'),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
        //SELECT * FROM `employee` LIMIT 10
        return $this->render('index', ['dataProvider' => $dataProvider]);
    }

    public function actionEmployee($id = null) {
        $model = new Employee();
        if ($id && $find = Employee::findBySql("SELECT * FROM employee WHERE id = $id LIMIT 1")) { //SELET * FROM employee WHERE id=1 LIMIT 1
            $model = $find;
            if (Yii::$app->request->get('delete')) {
                if ($model->delete()) { //DELETE FROM employee WHERE id = 1;
                    Yii::$app->session->setFlash('employeeSubmitted');
                    return $this->redirect(['admin/index']);
                }
            }
        }
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('employeeSubmitted');
            return $this->redirect(['admin/index']);
        }
        return $this->render('_employee', [
                    'model' => $model,
        ]);
    }

    public function actionOverview() {
        $stats = [];
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand('SELECT COUNT(*) FROM product');
        $numberOfProducts = $command->queryScalar();
        
        $orderItems = Basketitem::findBySql("SELECT * FROM `basketitem`")->all(); //SELECT * FROM `basketitem`
        $sum = 0;
        foreach ($orderItems as $item) {
            $sum += $item->quantity;
        }
        $numberOfOrders = $sum;
        $stats['numberOfProducts'] = $numberOfProducts;
        $stats['numberOfOrders'] = $numberOfOrders;
        return $this->render('overview', ['stats' => $stats]);
    }

    public function actionSales() {
        $fromdate = !empty(Yii::$app->request->get('from_date')) ? strtotime(Yii::$app->request->get('from_date')) : time();
        $todate = !empty(Yii::$app->request->get('to_date')) ? strtotime(Yii::$app->request->get('to_date')) : time();
        $todateUsed = strtotime('+1 day', $todate);
        $stats = [];
         //SELECT * FROM `basket` WHERE (updated_at>=1464127200) AND (updated_at<=1464213600)
        $baskets = Basket::findBySql('SELECT * FROM basket WHERE (updated_at>='. $fromdate.') AND (updated_at<='. $todateUsed.')')->all();
        $orderCount = count($baskets);
        $numberOfProductsSold = 0;
        $totalPrice = 0;
        $profit = 0;
        $tax = 0;
        foreach ($baskets as $basket) {
            $numberOfProductsSold = $basket->itemcount;
            $profit += $basket->profit;
            $totalPrice += $basket->totalcost;
            $tax += $basket->tax;
        }

        $stats = [
            'orders' => $orderCount,
            'products' => $numberOfProductsSold,
            'price' => $totalPrice,
            'profit' => $profit,
            'tax' => $tax,
        ];
        return $this->render('sales', ['stats' => $stats, 'baskets' => $baskets, 'from_date' => $fromdate, 'to_date' => $todate]);
    }

}
