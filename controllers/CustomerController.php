<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use app\models\Product;
use yii\data\ActiveDataProvider;
use app\models\Basketitem;
use app\models\Basket;

class CustomerController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex() {
        $dataProvider = new ActiveDataProvider([
            'query' => Product::findBySql("SELECT * FROM product"),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $this->render('index', ['dataProvider' => $dataProvider]);
    }

    public function actionProduct($id = null) {
        $model = new Basketitem();
        if ($id && $find = Product::findBySql('SELECT * FROM product WHERE id = '.$id.' LIMIT 1')->one()) { //SELECT * FROM product WHERE id = $id LIMIT 1
            $product = $find;
        } else {
            Yii::$app->session->setFlash('Error');
            return $this->redirect(['customer/index']);
        }
        $model->product_id = $product->id;
        if ($model->load(Yii::$app->request->post()) && $model->save()) { // INSERT query
            Yii::$app->session->setFlash('Submitted');
            return $this->redirect(['customer/index']);
        }
        $product->customerprice = Yii::$app->formatter->asCurrency($product->customerprice);
        return $this->render('_product', [
                    'model' => $model,
                    'product' => $product,
        ]);
    }

    public function actionBasket() {
        if (!$basket = Yii::$app->user->identity->hasActiveBasket()) {
            return $this->redirect(['customer/index']);
        }
        return $this->render('basket', ['basket' => $basket]);
    }

    public function actionCheckout() {
        if (!$basket = Yii::$app->user->identity->hasActiveBasket()) {
            return $this->redirect(['customer/index']);
        }
        $basket->totalcost = $basket->totalPrice();
        $basket->status = Basket::STATUS_COMPLETE;
        $basket->save();
        Yii::$app->session->setFlash('Checkout');
        return $this->redirect(['customer/index']);
    }

}
