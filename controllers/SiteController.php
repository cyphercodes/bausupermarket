<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use app\models\LoginForm;
use app\models\User;
use app\models\SignupForm;

class SiteController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['login', 'logout', 'index'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'login'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['logout'],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex() {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }
        if (Yii::$app->user->identity->type == User::TYPE_ADMIN) {
            return $this->redirect(['admin/index']);
        } else if (Yii::$app->user->identity->type == User::TYPE_DATAENTRY) {
            return $this->redirect(['dataentry/index']);
        } else {
            return $this->redirect(['customer/index']);
        }
    }

    public function actionLogin() {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
                    'model' => $model,
        ]);
    }

    public function actionLogout() {
        Yii::$app->user->logout();
        return $this->goHome();
    }
    
    public function actionSignup(){
        $model = new SignupForm();
        if($model->load(Yii::$app->request->post()) && $model->signup()){
              return $this->goBack();
        }
        return $this->render('signup', [
                    'model' => $model,
        ]);
    }

}
