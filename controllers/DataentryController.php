<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use app\models\Product;
use yii\data\ActiveDataProvider;

class DataentryController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex() {
        $dataProvider = new ActiveDataProvider([
            'query' => Product::find(),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $this->render('index', ['dataProvider' => $dataProvider]);
    }

    public function actionProduct($id = null) {
        $model = new Product();
        if ($id && $find = Product::findBySql('SELECT * FROM product WHERE id = '.$id.' LIMIT 1')->one()) {
            $model = $find;
            if (Yii::$app->request->get('delete')) {
                if ($model->delete()) {
                    Yii::$app->session->setFlash('employeeSubmitted');
                    return $this->redirect(['dataentry/index']);
                }
            }
        }
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('Submitted');
            return $this->redirect(['dataentry/index']);
        }
        return $this->render('_product', [
                    'model' => $model,
        ]);
    }

}
