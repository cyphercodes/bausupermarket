$(document).ready(function () {
    $('select').material_select();

    $(document).on('click', '#employee-list tbody tr', function () {
        var id = $(this).data('key');
        window.location.href = BASE_URL + '/admin/employee/' + id;
    });

    $(document).on('click', '#data-entry-list tbody tr', function () {
        var id = $(this).data('key');
        window.location.href = BASE_URL + '/dataentry/product/' + id;
    });

    $(document).on('click', '#product-list tbody tr', function () {
        var id = $(this).data('key');
        window.location.href = BASE_URL + '/customer/product/' + id;
    });

    $(".button-collapse").sideNav();


});
