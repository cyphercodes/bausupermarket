<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Signup';
?>
<h4 class="center teal-text">Signup</h4>
<div class="card-panel white">
    <?php
    $form = ActiveForm::begin([
                'id' => 'signup-form',
    ]);
    ?>

    <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>

    <?= $form->field($model, 'password')->passwordInput() ?>
    
    <?= $form->field($model, 'firstname') ?>
     <?= $form->field($model, 'lastname') ?>


    <div class="center">
        <?= Html::submitButton('Register', ['class' => 'waves-effect waves-light btn', 'name' => 'Register-button']) ?>
    </div>
    <div class="center margin-top-20">
    <?= Html::a("Already have an account", ['site/login']); ?>
    </div>
    <?php ActiveForm ::end(); ?>

</div>
