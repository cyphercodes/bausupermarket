<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

$this->title = 'My Basket';
?>
<h4 class="center teal-text">My Basket</h4>
<div class="card-panel white">
    <div class="card-content">
        <table class="responsive-table highlight">
            <thead>
                <tr>
                    <th data-field="id">Item Name</th>
                    <th data-field="name">Quantity</th>
                    <th data-field="price">Price</th>
                </tr>
            </thead>

            <tbody>
                <?php foreach ($basket->basketitems as $item) { ?>
                    <tr>
                        <td><?= $item->product->title ?></td>
                        <td><?= $item->quantity ?></td>
                        <td><?= Yii::$app->formatter->asCurrency($item->price) ?></td>
                    </tr>
                <?php } ?>
            </tbody>
            <tfoot>
                <tr>
                    <th></th>
                    <th></th>
                    <th data-field="totalprice"><?= Yii::$app->formatter->asCurrency($basket->totalPrice()) ?></th>
                </tr>
            </tfoot>
        </table>
        <div class="center">
            <?= Html::a('<i class="material-icons left">payment</i> Checkout', ['/customer/checkout'], ['class' => 'waves-effect waves-light btn']) ?>
        </div>
    </div>
</div>