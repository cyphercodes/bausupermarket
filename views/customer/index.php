<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

$this->title = 'Buy Products';
?>
<?php if (Yii::$app->session->hasFlash('Submitted')) { ?>
    <div class="card-panel green">
        <span class="white-text">
            Product added to basket!
        </span>
    </div>
<?php } ?>
<?php if (Yii::$app->session->hasFlash('Error')) { ?>
    <div class="card-panel red">
        <span class="white-text">
            Error no product!
        </span>
    </div>
<?php } ?>
<?php if (Yii::$app->session->hasFlash('Checkout')) { ?>
    <div class="card-panel green">
        <span class="white-text">
            Order Completed Successfully!
        </span>
    </div>
<?php } ?>
<h4 class="center teal-text">Product List</h4>
<div class="card-panel white">
    <div class="card-content">
        <?php
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'tableOptions' => ['class' => 'highlight centered responsive-table', 'id' => 'product-list'],
            'columns' => [
                'id',
                'title',
                [
                    'attribute' => 'customerprice',
                    'format' => 'Currency',
                ],
                [
                    'attribute' => 'expiry_date',
                    'format' => ['date', 'php:Y-m-d']
                ],
            ],
        ]);
        ?>
    </div>
</div>