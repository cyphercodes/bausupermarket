<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\models\Pricing;

$this->title = 'Add To Basket';
?>
<h4 class="center teal-text">Buy Product</h4>
<div class="card-panel white">
    <div class="card-content">
        <?php
        $form = ActiveForm::begin([
                    'id' => 'add-to-basket-form',
        ]);
        ?>
        <?= $form->field($product, 'title')->textInput(['disabled' => 'disabled']); ?>
        <?= $form->field($product, 'customerprice')->textInput(['disabled' => 'disabled']); ?>
        <?= $form->field($model, 'quantity'); ?>

        <div class="center">
            <?= Html::submitButton('Add to basket', ['class' => 'waves-effect waves-light btn']) ?>
        </div>

        <?php ActiveForm ::end(); ?>
    </div>
</div>