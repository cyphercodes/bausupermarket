<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
        <script>
            var BASE_URL = '<?= Url::base(true); ?>';
        </script>
    </head>
    <body>
        <?php $this->beginBody() ?>


        <nav class="teal">
            <div class="nav-wrapper container">
                <a href="<?= Url::toRoute(['/site/index']); ?>" class="brand-logo">BAU Supermarket</a>
                <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
                <ul id="nav-mobile" class="right hide-on-med-and-down">
                    <li><a href="<?= Url::toRoute(['/site/index']); ?>">Home</a></li>
                    <?php if (!Yii::$app->user->isGuest && Yii::$app->user->identity->isAdmin()) { ?>
                        <li><a href="<?= Url::toRoute(['/admin/overview']); ?>">Overview</a></li>
                        <li><a href="<?= Url::toRoute(['/admin/sales']); ?>">Sales</a></li>
                    <?php } ?>
                    <?php if (Yii::$app->user->isGuest) { ?>
                        <li><a href="<?= Url::toRoute(['/site/login']); ?>">Login</a></li>
                    <?php } else { ?>
                        <li><a href="<?= Url::toRoute(['/site/logout']); ?>">logout</a></li>
                    <?php } ?>
                </ul>
                <ul class="side-nav" id="mobile-demo">
                    <li><a href="<?= Url::toRoute(['/site/index']); ?>">Home</a></li>
                    <?php if (Yii::$app->user->isGuest) { ?>
                        <li><a href="<?= Url::toRoute(['/site/login']); ?>">Login</a></li>
                    <?php } else { ?>
                        <li><a href="<?= Url::toRoute(['/site/logout']); ?>">logout</a></li>
                    <?php } ?>
                </ul>
            </div>
        </nav>
        <main>
            <div class="container">
                <?= $content ?>
                <?php if (!Yii::$app->user->isGuest && Yii::$app->user->identity->hasActiveBasket()) { ?>
                    <div class="fixed-action-btn" style="top: 20px; right: 104px;">
                        <?= Html::a('<i class="large material-icons">shopping_cart</i>', ['/customer/basket'], ['class' => 'btn-floating btn-large teal lighten-2']); ?>

                    </div>
                    <?php
                }
                ?>
            </div>
        </main>

        <footer class="page-footer teal">
            <div class="footer-copyright teal">
                <div class="container">
                    © <?= date('Y') ?> BAU Supermarket
                    <span class="grey-text text-lighten-4 right">Created by Rawad Salhab</span>
                </div>
            </div>
        </footer>

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
