<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\models\Pricing;

$this->title = 'Data Entry Section';
?>
<h4 class="center teal-text"><?= $model->isNewRecord ? 'Add' : 'Edit'; ?> Product</h4>
<div class="card-panel white">
    <div class="card-content">
        <?php
        $form = ActiveForm::begin([
                    'id' => 'add-employee-form',
        ]);
        ?>
        <?= $form->field($model, 'title') ?>
        <?= $form->field($model, 'price')->textInput(['placeholder' => 'In USD']) ?>
        <?= $form->field($model, 'expiry_date')->widget(\yii\jui\DatePicker::classname()); ?>
        <?= $form->field($model, 'tax')->textInput(['placeholder' => 'In Percentage']); ?>
        <?= $form->field($model, 'profit_margin')->textInput(['placeholder' => 'In Percentage (Example: 35 = 35%, price will be multiplied by 1.35']); ?>
        <div class="center">
            <?= Html::submitButton($model->isNewRecord ? 'Add' : 'Edit', ['class' => 'waves-effect waves-light btn']) ?>
            <?php if (!$model->isNewRecord) { ?>
                <?= Html::a('Delete', Url::base(true) . '/dataentry/product/' . $model->id . '?delete=delete', ['class' => 'waves-effect waves-light btn red']) ?>
            <?php } ?>
        </div>

        <?php ActiveForm ::end(); ?>
    </div>
</div>