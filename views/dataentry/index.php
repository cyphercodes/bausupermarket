<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

$this->title = 'Data Entry Section';
?>
<?php if (Yii::$app->session->hasFlash('Submitted')) { ?>
    <div class="card-panel green">
        <span class="white-text">
            Changes Saved!
        </span>
    </div>
<?php } ?>
<h4 class="center teal-text">Product List</h4>
<div class="card-panel white">
    <div class="card-content">
        <?php
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'tableOptions' => ['class' => 'highlight centered responsive-table', 'id' => 'data-entry-list'],
            'columns' => [
                'id',
                'title',
                [
                    'attribute' => 'price',
                    'format' => 'Currency',
                ],
                [
                    'attribute' => 'expiry_date',
                    'format' => ['date', 'php:Y-m-d']
                ],
                'tax',
                [
                    'attribute' => 'profit_margin',
                ],
                [
                    'attribute' => 'created_at',
                    'format' => ['date', 'php:Y-m-d H:i']
                ],
            ],
        ]);
        ?>
        <hr />
        <div class="center">
            <?= Html::a('<i class="material-icons left">add</i> Add Product', ['/dataentry/product'], ['class' => 'waves-effect waves-light btn']) ?>
        </div>
    </div>
</div>