<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\models\Pricing;

$this->title = 'Admin Section';
?>
<h4 class="center teal-text"><?= $model->isNewRecord ? 'Add' : 'Edit'; ?> Employee</h4>
<div class="card-panel white">
    <div class="card-content">
        <?php
        $form = ActiveForm::begin([
                    'id' => 'add-employee-form',
        ]);
        ?>
        <?= $form->field($model, 'firstname') ?>
        <?= $form->field($model, 'lastname') ?>
        <?= $form->field($model, 'gender')->dropDownList(['Male' => 'Male', 'Female' => 'Female']) ?>
        <?= $form->field($model, 'salary'); ?>
        <div class="center">
            <?= Html::submitButton($model->isNewRecord ? 'Add' : 'Edit', ['class' => 'waves-effect waves-light btn']) ?>
            <?php if (!$model->isNewRecord) { ?>
                <?= Html::a('Delete', Url::base(true) . '/admin/employee/' . $model->id . '?delete=delete', ['class' => 'waves-effect waves-light btn red']) ?>
            <?php } ?>
        </div>

        <?php ActiveForm ::end(); ?>
    </div>
</div>