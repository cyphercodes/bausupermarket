<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

$this->title = 'Admin Section';
?>

<h4 class="center teal-text">Overview Report</h4>
<div class="row">
    <div class="col s6 m6">
        <div class="card-panel white">
            <span>
                <strong><?= $stats['numberOfProducts'] ?></strong> total products available
            </span>
        </div>
    </div>
    <div class="col s6 m6">
        <div class="card-panel white">
            <span>
                <strong><?= $stats['numberOfOrders'] ?></strong> total products sold
            </span>
        </div>
    </div>
</div>
