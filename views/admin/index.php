<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

$this->title = 'Admin Section';
?>
<?php if (Yii::$app->session->hasFlash('employeeSubmitted')) { ?>
    <div class="card-panel green">
        <span class="white-text">
            Changes Saved!
        </span>
    </div>
<?php } ?>
<h4 class="center teal-text">Employee List</h4>
<div class="card-panel white">
    <div class="card-content">
        <?php
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'tableOptions' => ['class' => 'highlight centered responsive-table', 'id' => 'employee-list'],
            'columns' => [
                'id',
                'firstname',
                'lastname',
                'gender',
                [
                    'attribute' => 'salary',
                    'format' => 'Currency',
                ],
                [
                    'attribute' => 'created_at',
                    'format' => ['date', 'php:Y-m-d H:i']
                ],
            ],
        ]);
        ?>
        <hr />
        <div class="center">
            <?= Html::a('<i class="material-icons left">add</i> Add Employee', ['/admin/employee'], ['class' => 'waves-effect waves-light btn']) ?>
        </div>
    </div>
</div>