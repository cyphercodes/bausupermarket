<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\jui\DatePicker;

$this->title = 'Admin Section';
?>

<h4 class="center teal-text">Sales Report</h4>
<form method="GET">
    <div class="row">
        <div class="col s6 m6">
            <label>From</label>
            <?=
            DatePicker::widget([
                'name' => 'from_date',
                'value' => $from_date,
            ]);
            ?>
        </div>
        <div class="col s6 m6">
            <label>To</label>
            <?=
            DatePicker::widget([
                'name' => 'to_date',
                'value' => $to_date,
            ]);
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col s12 m12 center">
            <button type="submit" class="waves-effect waves-light btn">Search</button>
        </div>
    </div>
    <!--<hr />-->
    <h5 class="teal-text">Search Results:</h5>
    <div class="row">
        <div class="col s6 m6">
            <div class="card-panel white">
                <span>
                    Number of Orders: <strong><?= $stats['orders']; ?></strong>
                </span>
            </div>
        </div>
        <div class="col s6 m6">
            <div class="card-panel white">
                <span>
                    Number of Products Sold: <strong><?= $stats['products']; ?></strong>
                </span>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col s4 m4">
            <div class="card-panel white">
                <span>
                    Total Price: <strong><?= Yii::$app->formatter->asCurrency($stats['price']); ?></strong>
                </span>
            </div>
        </div>
        <div class="col s4 m4">
            <div class="card-panel white">
                <span>
                    Total Profit: <strong><?= Yii::$app->formatter->asCurrency($stats['profit']); ?></strong>
                </span>
            </div>
        </div>
        <div class="col s4 m4">
            <div class="card-panel white">
                <span>
                    Total Tax: <strong><?= Yii::$app->formatter->asCurrency($stats['tax']); ?></strong>
                </span>
            </div>
        </div>
    </div>
</form>

